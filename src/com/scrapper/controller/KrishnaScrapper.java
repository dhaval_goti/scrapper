package com.scrapper.controller;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class KrishnaScrapper
{

	public static void main(String[] args) throws Exception
	{
		// Document doc = Jsoup.connect("https://www.thehomesecuritysuperstore.com/self-defense-stun-guns-taser-taser-cartridge-pouch-with-clip-44934-p=5441").get();

		// Elements ele = doc.getElementsByClass("breadcrumb");
		// Elements ele = doc.getElementsByTag("li");

		// Element liElemets = ele.get(0);

		// for (Element liEle : ele.get(0).getElementsByTag("li"))
		// {
		// System.out.println(liEle.text());
		// }
		// System.out.println(ele.get(0).getElementsByTag("li").get(0).text());

		try
		{

			// Create an object of filereader
			// class with CSV file as a parameter.
			FileReader filereader = new FileReader("F:\\Large_Sheet.csv");

			// create csvReader object passing
			// file reader as a parameter
			CSVReader csvReader = new CSVReader(filereader);
			String[] nextRecord;
			CSVWriter writer = new CSVWriter(new FileWriter(new File("F:\\test.csv")));

			// we are going to read data line by line
			int i = 1;
			int failCount = 0;
			while ((nextRecord = csvReader.readNext()) != null)
			{

				for (String cell : nextRecord)
				{
					String breadCumbText = "";
					try
					{
						Document doc = Jsoup.connect("https://www.thehomesecuritysuperstore.com" + cell.trim()).get();
						Elements ele = doc.getElementsByClass("breadcrumb");

						for (int l = 0; l < ele.get(0).getElementsByTag("li").size() - 1; l++)
						{
							if (l == ele.get(0).getElementsByTag("li").size() - 2)
							{
								breadCumbText = breadCumbText + ele.get(0).getElementsByTag("li").get(l).text();
							}
							else
							{
								breadCumbText = breadCumbText + ele.get(0).getElementsByTag("li").get(l).text() + " > ";
							}
						}
						// for (Element liEle : ele.get(0).getElementsByTag("li"))
						// {
						// breadCumbText = breadCumbText + liEle.text() + " > ";
						// }

					}
					catch (Exception e)
					{
						// System.out.println(" " + i);
						// e.printStackTrace();
						breadCumbText = "Page not found";
						failCount++;
					}
					System.out.println(" " + i + " " + breadCumbText);
					// try (PrintWriter writer = new PrintWriter(new File("F:\\test.csv")))
					// {

					String[] header = {breadCumbText, "Marks"};
					writer.writeNext(header);

					// System.out.println("done!");

					// }
					// catch (FileNotFoundException e)
					// {
					// System.out.println(e.getMessage());
					// }

					i++;
				}
			}
			System.out.println(" Count - " + failCount);
			writer.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}
}
