package com.scrapper.controller;

import java.io.FileReader;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import com.opencsv.CSVReader;

public class CountFinder
{

	public static void main(String[] args)
	{

		int count = 0;
		try
		{
			FileReader filereader = new FileReader("F:\\collection_sublevel_only_product.csv");

			CSVReader csvReader = new CSVReader(filereader);
			String[] nextRecord;
			String url = "";

			while ((nextRecord = csvReader.readNext()) != null)
			{
				// create csvReader object passing
				// file reader as a parameter

				for (String cell : nextRecord)
				{
					count++;
					Document doc = Jsoup.connect("https://www.thehomesecuritysuperstore.com" + cell.trim()).get();
					String text = doc.getElementsByTag("h1").get(0).text();
					if (text.contains("(") && text.contains(")"))
						System.out.println("Count - " + count + " - " + text.substring(text.indexOf("(") + 1, text.indexOf(")")));

				}

			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			count++;
		}

	}
}
