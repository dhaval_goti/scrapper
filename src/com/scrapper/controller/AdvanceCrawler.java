package com.scrapper.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class AdvanceCrawler {

	public static void main(String[] args) {
		try {
		Response loginFormFirst = Jsoup.connect("https://demo-2009.myshopify.com/admin/login/auth/")
					.userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0")
					.header("Accept-Language", "text/html")
					.followRedirects(false)
					.execute();	
		System.out.println(" First Header - "+loginFormFirst.headers());
		System.out.println(" First Location - "+loginFormFirst.header("Location"));
		
		String originalUrl = Jsoup.connect("https://demo-2009.myshopify.com/admin/login/auth/")
                .followRedirects(true) //to follow redirects
                .execute().url().toExternalForm();
		System.out.println(" originalUrl - "+originalUrl);
		Response loginFormSecond = Jsoup.connect("https://demo-2009.myshopify.com/admin/login/auth")
				.userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0")
				.header("Accept-Language", "text/html")
				.header("Accept-Language", "application/xhtml+xml")
				.cookies(loginFormFirst.cookies())
				.followRedirects(false)
				.execute();	
		
		System.out.println(" Second Header - "+loginFormSecond.headers());

		System.out.println(" Second URL - "+loginFormSecond.header("Location"));
		System.out.println(" First Location - "+loginFormSecond.header("Location"));
			Document doc = loginFormFirst.parse();

			Elements elements = doc.getElementsByTag("input");

			String token = "";
			for (Element ele : elements) {
				Attributes attributes = ele.attributes();

				for (Attribute attirb : attributes) {
					if (attirb.getKey().equals("value") && attirb.getValue().contains("==")) {
						token = attirb.getValue();
					}
				}
			}


//		Response resposne =  Jsoup.connect("https://demo-2009.myshopify.com/admin/login/auth/")
//		        .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0")         
//		        .data("login", "niraj@planetx.in")
//		        .data("password", "#Figure8out")
//		        .execute();

			Document doc1 = Jsoup.connect("https://demo-2009.myshopify.com/admin/auth/login")
					.userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0")
					.data("login", "niraj@planetx.in").data("password", "#Figure8out").data("authenticity_token", token)
					.cookies(loginFormSecond.cookies()).header("X-Shopify-Web", "1").post();

			FileOutputStream fileOut = null;
			try {
				File file = new File("File1.html");

				fileOut = new FileOutputStream(file);

				fileOut.write(doc1.getElementsByTag("html").get(0).text().getBytes(), 0,
						doc1.getElementsByTag("html").get(0).text().getBytes().length);
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					fileOut.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

//		Document doc2=  Jsoup.connect("https://demo-2009.myshopify.com/admin/settings/files")
//		        .userAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0")         
//		        .data("login", "niraj@planetx.in")
//		        .data("password", "#Figure8out")
//		        .cookies(resposne.cookies())
//		        .header("X-Shopify-Web", "1")
//		        .post();

			System.out.println(" Success - ");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
