package com.scrapper.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

@WebServlet({ "/CrateJoy" })
public class CrateJoyController extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private ServletContext servletContext;
	private static Logger logger = Logger.getLogger(CrateJoyController.class);
	public static String cookieForBloomsy = "session=b'732d71d3-3eef-4711-a5e2-115e4c956bc3'; LD_T=70f82864-dcbd-4d4b-88b7-5f236ecca0b3; _ga=GA1.2.969789885.1539761492; _gid=GA1.2.2107132445.1539761492; statjoy_metrics={%22errors%22:{}%2C%22errorCount%22:0%2C%22callCount%22:{%22init%22:4%2C%22track%22:5%2C%22registerOnce%22:5%2C%22login%22:1}%2C%22successCount%22:{%22init%22:4%2C%22track%22:5%2C%22login%22:1}%2C%22failureCount%22:{}}; LD_U=https%3A%2F%2Fmy.cratejoy.com%2Fv%2Flogin%3Fq%3Dhttps%253A%252F%252Fmy.cratejoy.com%252Fcoupons%252F%23page%3D1; LD_R=; LD_T=70f82864-dcbd-4d4b-88b7-5f236ecca0b3; fs_uid=rs.fullstory.com`5ZK24`6305729055555584:5685265389584384`1634797597`; statjoy_session={%22userId%22:1634797597%2C%22uuid%22:%22e5b69cd2-7437-4c11-bbf9-0f3616dd8b36%22%2C%22appName%22:%22vendor_client%22%2C%22sessionId%22:%22732d71d3-3eef-4711-a5e2-115e4c956bc3%22%2C%22registeredProps%22:{}}; __ar_v4=TRZ26KFZGFGRPISADZZOI7%3A20181016%3A4%7CB7B7JO3SFNBQJMUA3QHDPS%3A20181016%3A4%7CQOM6IDI7QND4VAHIFCY5QK%3A20181016%3A4; LD_S=1539778340116; existing_merchant=a%2Bbloomsy%40wlfpt.co; LD_L=%7B%22first_name%22%3A%22WolfPoint%22%2C%22last_name%22%3A%22Agency%22%2C%22company%22%3A%22BloomsyBox%22%2C%22email%22%3A%22a%2Bbloomsy%40wlfpt.co%22%7D; __zlcmid=ovhirha4J6W65B; intercom-session-41cc7959d258a6b217d925d6bdccaf896309ba00=RlRBQjFtVlp3SFVRWW8wVllCSTVzbmNub2h6QjV6QUtQMU55dlFuK3A2NXNxYzlxVjJuS2l3N1IxWkwvSXpwSC0tY3hWcXRtS3BLK01tTjVTaWZaU2hUdz09--f64c5d1325fb8fe75067386daf9f490ff43f6882";
	public static String cookieForLittlePoppy = "LD_T=08f12988-9207-4ecf-8760-9d8ddf98a3dc; _ga=GA1.2.1855858102.1540884419; fs_uid=rs.fullstory.com`5ZK24`5138431124963328:5629499534213120`1815669541`; statjoy_metrics={%22errors%22:{}%2C%22errorCount%22:0%2C%22callCount%22:{%22init%22:14%2C%22track%22:33%2C%22registerOnce%22:33%2C%22track_resume%22:1%2C%22login%22:2%2C%22logout%22:1}%2C%22successCount%22:{%22init%22:14%2C%22track%22:31%2C%22track_resume%22:1%2C%22login%22:2%2C%22logout%22:1}%2C%22failureCount%22:{%22track%22:1}}; LD_U=https%3A%2F%2Fmy.cratejoy.com%2Fv%2Flogin%3Fq%3Dhttps%253A%252F%252Fmy.cratejoy.com%252Fcoupons%252Fcoupons_list%252F%253Fpage%253D1%2526per_page%253D1000%2526total_pages%253D1%2526total_entries%253D45%2526q%253D; LD_R=; LD_T=08f12988-9207-4ecf-8760-9d8ddf98a3dc; statjoy_session={%22userId%22:1815669541%2C%22uuid%22:%220f02f4e4-6523-43ad-a136-12a788b039d9%22%2C%22appName%22:%22vendor_client%22%2C%22sessionId%22:%2223630292-f87c-4e08-9a09-486a89f9511d%22%2C%22registeredProps%22:{%22$initial_referrer%22:%22https://my.cratejoy.com/refer/%22%2C%22$initial_referrer_href%22:%22https://my.cratejoy.com/refer/%22%2C%22$initial_referring_domain%22:%22my.cratejoy.com%22}}; intercom-session-41cc7959d258a6b217d925d6bdccaf896309ba00=SGsyY0k3SVBaa08vQmpYT01COGIwUzJsUStUdzNSZ1ViS1BYYUY4YnhEenlOTHNISzNXb2liUmRWOWs4QzdhYy0tSTYzdUJMSUNRaXBtUHNZMkJ4UHpqZz09--692e0ae32c4219b6f0b0edecf97f5877098ec32a; __zlcmid=pAhl8c8zGgLGVc; session=b'23630292-f87c-4e08-9a09-486a89f9511d'; _gid=GA1.2.2128193374.1541218026; _fbp=fb.1.1541218026501.1133602583; existing_merchant=a%2Bpoppy%40wlfpt.co; LD_L=%7B%22first_name%22%3A%22Wolf%22%2C%22last_name%22%3A%22Point%22%2C%22company%22%3A%22Little%20Poppy%20Co.%22%2C%22email%22%3A%22a%2Bpoppy%40wlfpt.co%22%7D; last_seen=1541218043045\"";

	public void init(ServletConfig Conf) throws ServletException {
		super.init(Conf);
		try {
			servletContext = Conf.getServletContext();

			String log4jConfigFile = servletContext.getInitParameter("log4j-config-location");
			String fullPath = servletContext.getRealPath("") + File.separator + log4jConfigFile;

			org.apache.log4j.PropertyConfigurator.configure(fullPath);
		} catch (Exception ex) {
			logger.error("Failed to get servletContext", ex);
			throw ex;
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println(" In doGet Method");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println(" In doPost Method");

		doCratejoyReview(request, response);
	}

	private void doCratejoyReview(HttpServletRequest request, HttpServletResponse response) {
		try {
			String fileName = "coupons";
			String product = request.getParameter("product");
			String toDateString = request.getParameter("datepicker");
			SimpleDateFormat toDateFormatter = new SimpleDateFormat("MM/dd/yyyy");
			Date toDate = null;
			if(toDateString != null && !toDateString.isEmpty())
			{
				toDate = toDateFormatter.parse(toDateString);
			}
			File output = File.createTempFile("coupons", ".csv");

			OutputStream outStream = response.getOutputStream();

			// args = new String[1];
			// args[0] = "E:\\coupons.csv";

			BufferedWriter out = new BufferedWriter(
					new OutputStreamWriter(new FileOutputStream(output.getPath().replaceAll("\"", "")), "UTF8"));

			byte[] buffer = new byte[2048];
			int bytesRead = -1;
			InputStream is = new FileInputStream(output);
			String header = "Total Redeemed,Description,Coupon Code,Active?,Coupon Name,URL,Discount Type,Free Shipping,Min Order Price,Multiplier (%),Reduction ($ Amount),Activation Date,Expiration Date,Max Redemptions,Once Per Customer,One Item Per Cart,Exclusive,# of Renewals,Recurring Type,Selected Products\r\n";
			out.write(header, 0, header.length());
			// out.write(
			// "Total Redeemed,Description,Coupon Code,Active?,Coupon Name,URL,Discount
			// Type,Free Shipping,Min Order Price,Multiplier (%),Reduction ($
			// Amount),Activation Date,Expiration Date,Max Redemptions,Once Per Customer,One
			// Item Per Cart,Exclusive,# of Renewals,Recurring Type,Selected Products\r\n");

			int counter = 0;
			String h = getHTML(
					"https://my.cratejoy.com/coupons/coupons_list/?page=1&per_page=1000&total_pages=1&total_entries=45&q=",product);
			h += "{\"all_sub_prod_enabled";
			int noOfCount = 0;
			while (true) {
				String row = extractDataWithHTML(h, "{\"all_sub_prod_enabled", "{\"all_sub_prod_enabled");
				if (row.equals(""))
					break;
				h = h.substring(h.indexOf("{\"all_sub_prod_enabled") + 5);

				List<String> list = new ArrayList<String>();

				list.add(extractData(row, "\"total_redeemed\":", ","));
				list.add(extractData(row, "\"description\": \"", "\""));
				list.add(extractData(row, "\"primary_code\": \"", "\""));
				list.add(extractData(row, "\"active\":", ","));
				list.add(extractData(row, "\"name\": \"", "\""));
				System.out.println(extractData(row, "\"created_at\": \"", "\""));
				list.add(extractData(row, "\"created_at\": \"", "\""));
				String dateString = extractData(row, "\"created_at\": \"", "\"");
				dateString = dateString.substring(0, 10);
				String format = "yyyy-MM-dd";

				SimpleDateFormat formatter = new SimpleDateFormat(format);
				Date createdDate = formatter.parse(dateString);
				
				if (toDate == null || toDate.before(createdDate)) {
					String id = extractData(row, "\"coupon_id\":", "}");
					list.add("https://my.cratejoy.com/coupon/" + id);
					String h1 = getHTML("https://my.cratejoy.com/coupon/" + id,product);

					h1 = extractDataWithHTML(h1, "window.coupon =", "window.cj.has_ecom");
					String h2 = extractDataWithHTML(h1, "\"discount\": {", "}");

					String type = extractData(h2, "\"discount_type\":", ",");
					if (type.equals("1"))
						type = "No Discount";
					else if (type.equals("3"))
						type = "Percentage";
					else if (type.equals("2"))
						type = "Flat Rate";
					else if (type.equals("4"))
						type = "Min Order Price";
					else if (type.equals("5"))
						type = "Free Shipping";

					list.add(type);
					list.add(extractData(h2, "\"free_shipping\":", ","));
					list.add(extractData(h2, "\"min_price\":", ","));
					list.add(extractData(h2, "\"multiplier\":", ","));
					list.add(extractData(h2, "\"reduction\":", ","));

					h2 = extractDataWithHTML(h1, "\"limits\": {", "}");
					String date = extractData(h2, "\"activation_date\":", ",");
					String active = "";
					try {
						Date dt = new Date(Long.parseLong(date));

						String DATE_FORMAT2 = "MM/dd/yyyy";
						java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT2);

						active = sdf.format(dt);

					} catch (Exception ex) {

					}

					date = extractData(h2, "\"expiration_date\":", ",");
					String exp = "";
					try {
						Date dt = new Date(Long.parseLong(date));

						String DATE_FORMAT2 = "MM/dd/yyyy";
						java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT2);

						exp = sdf.format(dt);
					} catch (Exception ex) {

					}

					list.add(active);
					list.add(exp);

					String max = extractData(h2, "\"max_redemptions\":", ",");
					if (max.equals("null"))
						max = "";
					list.add(max);
					list.add(extractData(h2, "\"once_per_customer\":", ","));
					list.add(extractData(h2, "\"one_item_per_cart\":", ","));
					list.add(extractData(h2, "\"exclusive\":", ","));

					h2 = extractDataWithHTML(h1, "\"recurring\": {", "}");
					list.add(extractData(h2, "\"num_renewals\":", ","));
					String rType = extractData(h2, "\"recurring_type\":", ",");
					if (rType.equals("1"))
						rType = "Apply only once";
					else if (rType.equals("2"))
						rType = "Apply on every renewal";
					else if (rType.equals("3"))
						rType = "Fixed number of renewals";

					list.add(rType);

					h1 += "{\"coupon_id\":";
					String subscription = "", oneTime = "";
					while (true) {
						String row2 = extractDataWithHTML(h1, "{\"coupon_id\":", "{\"coupon_id\":");
						if (row2.equals(""))
							break;
						h1 = h1.substring(h1.indexOf("{\"coupon_id\":") + 5);

						String isEcom = extractData(row2, "\"is_ecom\":", ",");
						String productName = extractData(row2, "product_name\": \"", "\"");
						row2 = extractDataWithHTML(row2, "\"terms\": [", "]");
						int trueCount = 0, falseCount = 0;

						while (true) {
							String row3 = extractDataWithHTML(row2, "{", "}");
							if (row3.equals(""))
								break;
							row2 = row2.substring(row2.indexOf("{") + 5);

							String enabled = extractData(row3, "enabled\":", ",");
							if (enabled.equals("true"))
								trueCount++;
							else if (enabled.equals("false"))
								falseCount++;
						}
						if (trueCount > 0 && falseCount == 0) {
							if (isEcom.equals("false")) {
								subscription += productName + ", ";
							} else
								oneTime += productName + ", ";
						}
					}

					subscription = subscription.trim();
					oneTime = oneTime.trim();

					if (subscription.endsWith(","))
						subscription = subscription.substring(0, subscription.length() - 1).trim();
					if (oneTime.endsWith(","))
						oneTime = oneTime.substring(0, oneTime.length() - 1).trim();

					list.add(subscription);
					list.add(oneTime);

					String line = "";
					for (int k = 0; k < list.size(); k++) {
						String l = (String) list.get(k);
						line += "\"" + l.replaceAll("\"", "\"\"") + "\",";
					}

					if (line.endsWith(","))
						line = line.substring(0, line.length() - 1).trim();

					line = line + "\r\n";

					out.write(line, 0, line.length());

					counter++;
					System.out.println(counter);
					noOfCount++;
				}
			}
			out.close();

			// FileInputStream fileIn = new FileInputStream(output);

			InputStream fileIn = new FileInputStream(output);
			String mimeType = "application/octet-stream";

			response.setContentType(mimeType);
			response.setContentLength(fileIn.available());

			String headerKey = "Content-Disposition";
			String filename = fileName + "_Review" + ".csv";
			String headerValue = String.format("attachment; filename=\"%s\"", new Object[] { filename });
			response.setHeader(headerKey, headerValue);

			// byte[] buffer = new byte[2048];
			// int bytesRead = -1;

			while ((bytesRead = fileIn.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			is.close();
			fileIn.close();
			outStream.close();
			logger.info("Your excel coupen file has been generated!");
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(" Error Occured while getting CrateJoy Review.", e);
		}
	}

	public static String extractData(String html, String s2, String s3) {
		int i = html.indexOf(s2);
		String result = "";
		if (i >= 0) {
			html = html.substring(i + s2.length());
			int j = html.indexOf(s3);
			if (j >= 0) {
				result = html.substring(0, j);
			} else {
				return "";
			}
		}

		return result.replaceAll("&nbsp;", " ").replaceAll("\\<.*?\\>", "").replaceAll("\\s+", " ").trim();
	}

	public static String extractDataWithHTML(String html, String s2, String s3) {
		int i = html.indexOf(s2);
		String result = "";
		if (i >= 0) {
			html = html.substring(i + s2.length());
			int j = html.indexOf(s3);
			if (j >= 0) {
				result = html.substring(0, j);
			} else {
				return "";
			}
		}

		return result.replaceAll("&nbsp;", " ").replaceAll("\\s+", " ").trim();
	}

	public static String getHTML(String address,String product) {

		String html = "";
		for (int j = 0; j < 3; j++) {
			try {
				// Thread.sleep(2000);
				URLConnection conn = null;

				html = "";

				URL url = new URL(address);

				conn = url.openConnection();

				conn.setRequestProperty("User-Agent",
						"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0");
				if (product.contains("bloomsy")) {
					conn.setRequestProperty("Cookie", cookieForBloomsy);
				}

				if (product.contains("poppy")) {
					conn.setRequestProperty("Cookie", cookieForLittlePoppy);
				}

				try {
					InputStream is = conn.getInputStream();
					final Reader reader = new InputStreamReader(is, "UTF-8");
					final char[] buf = new char[16384];
					int read;
					final StringBuffer sb = new StringBuffer();
					while ((read = reader.read(buf)) > 0) {
						sb.append(buf, 0, read);
					}

					html = sb.toString();
				} finally {

				}

				break;
			} catch (Exception ex) {
				// ex.printStackTrace();
				if (ex.getClass().toString().indexOf("FileNotFoundException") >= 0) {
					return "XXX";
				}
			}
		}

		return html;
	}
}
