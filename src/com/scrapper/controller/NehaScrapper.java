package com.scrapper.controller;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class NehaScrapper
{

	public static void main(String[] args)
	{
		try
		{
			CSVWriter writer = new CSVWriter(new FileWriter(new File("F:\\abcd.csv")));
			FileReader filereader = new FileReader("F:\\collection_sublevel_only_product_10_products.csv");

			CSVReader csvReader = new CSVReader(filereader);
			String[] nextRecord;
			String url = "";
			int count = 1;

			while ((nextRecord = csvReader.readNext()) != null)
			{
				try
				{
					// create csvReader object passing
					// file reader as a parameter

					for (String cell : nextRecord)
					{
						Document doc = Jsoup.connect("https://www.thehomesecuritysuperstore.com" + cell.trim() + "?show=90").get();

						Elements paginations = doc.getElementsByClass("pagination");
						Elements liElements = paginations.get(0).getElementsByTag("li");
						if (liElements.size() > 0)
						{
							System.out.println(" In Pagination " + cell);
							System.out.println("In If Count - " + count + " Items Count - " + doc.getElementsByTag("h1").get(0).text());
							liElements.remove(0);
							liElements.remove(liElements.size() - 1);
							for (Element oneEle : liElements)
							{
								Document docNew;
								if (oneEle.getElementsByTag("a").get(0).attr("href").equals("#"))
								{
									docNew = doc;
								}
								else
								{
									System.out.println(" In else for Url");
									docNew = Jsoup.connect("https://www.thehomesecuritysuperstore.com" + oneEle.getElementsByTag("a").get(0).attr("href")).get();
								}

								Elements prodList = docNew.getElementsByClass("product-details");

								int urlCount = 0;
								for (Element prod : prodList)
								{
									url = url + "," + prod.getElementsByTag("a").get(0).attr("href");
									System.out.println("URL Count - " + urlCount++);
								}
							}

						}
						else
						{

							System.out.println(" Count - " + count + " Items Count - " + doc.getElementsByTag("h1").get(0).text());
							Elements prodList = doc.getElementsByClass("product-details");

							for (Element prod : prodList)
							{
								url = url + "," + prod.getElementsByTag("a").get(0).attr("href");
							}
						}

						// Elements prodList = doc.getElementsByClass("col-sm-6 col-md-4 product-list-item");

						// Element liElemets = prodList.get(0);
						// Elements divEle = liElemets.getElementsByClass("product-details");
						// System.out.println(divEle.get(0).getElementsByTag("a").get(0).attr("href"));

						// System.out.println(liElemets.html());

						// System.out.println(prodList.size());
						// System.out.println(liElemets.html());

						// for (Element liEle : ele.get(0).getElementsByTag("li"))
						// {
						// System.out.println(liEle.text());
						// }

						String[] header = {url};
						writer.writeNext(header);
						url = "";

					}
				}
				catch (Exception e)
				{
					e.printStackTrace();
					String[] header = {"Not Found"};
					writer.writeNext(header);
					url = "";
				}
				count++;
			}
			writer.close();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}

	}

}
