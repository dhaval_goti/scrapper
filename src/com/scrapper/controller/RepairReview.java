package com.scrapper.controller;

import java.io.File;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class RepairReview
{

	public static void main(String[] args)
	{

		try
		{
			// CSVWriter writer = new CSVWriter(new FileWriter(new File("E:\\test.csv")));
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Java Books");
			String fileName = "E:\\repairsuniverse.html";

			Document doc = Jsoup.parse(new File(fileName), "utf-8");
			Elements divTag = doc.getElementsByTag("table");

			String text = divTag.html();
			int count = 0;

			int rowCount = 0;
			for (int i = 0; i < 7; i++)
			{
				HSSFRow row = sheet.createRow(++rowCount);

				int colCount = 0;
				HSSFCell cell = row.createCell(++colCount);
				for (Element ele : divTag)
				{
					String review = ele.text();
					if (review != null && review.contains("Order:"))
					{
						int orderId = Integer.parseInt(review.substring(review.indexOf("Order:") + 6, review.indexOf("Rating:")).trim());
						if (orderId > 389496)
						{
							String rev = review.substring(review.indexOf("Body:") + 5).trim();
							String[] header = {String.valueOf(rev), "Marks"};
							// writer.writeNext(header);
							System.out.println(count++);
						}

					}
				}
				System.out.println(count);
				// writer.close();
				// Element table = divTag.get(0);

				// .out.println(table.html());

				// System.out.println(divTag.text());

				// System.out.println(" --- " + text);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

}
