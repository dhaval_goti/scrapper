package com.scrapper.controller;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

@WebServlet({ "/Scrap" })
public class ScrapperController extends HttpServlet {
	private static Pattern patternDomainName;
	private Matcher matcher;
	private static final String DOMAIN_NAME_PATTERN = "([a-zA-Z0-9]([a-zA-Z0-9\\-]{0,61}[a-zA-Z0-9])?\\.)+[a-zA-Z]{2,6}";
	static {
		patternDomainName = Pattern.compile(DOMAIN_NAME_PATTERN);
	}
	private static final long serialVersionUID = 1L;
	private static Logger logger = Logger.getLogger(ScrapperController.class);
	public static String cookie = "session=b'732d71d3-3eef-4711-a5e2-115e4c956bc3'; LD_T=70f82864-dcbd-4d4b-88b7-5f236ecca0b3; _ga=GA1.2.969789885.1539761492; _gid=GA1.2.2107132445.1539761492; statjoy_metrics={%22errors%22:{}%2C%22errorCount%22:0%2C%22callCount%22:{%22init%22:4%2C%22track%22:5%2C%22registerOnce%22:5%2C%22login%22:1}%2C%22successCount%22:{%22init%22:4%2C%22track%22:5%2C%22login%22:1}%2C%22failureCount%22:{}}; LD_U=https%3A%2F%2Fmy.cratejoy.com%2Fv%2Flogin%3Fq%3Dhttps%253A%252F%252Fmy.cratejoy.com%252Fcoupons%252F%23page%3D1; LD_R=; LD_T=70f82864-dcbd-4d4b-88b7-5f236ecca0b3; fs_uid=rs.fullstory.com`5ZK24`6305729055555584:5685265389584384`1634797597`; statjoy_session={%22userId%22:1634797597%2C%22uuid%22:%22e5b69cd2-7437-4c11-bbf9-0f3616dd8b36%22%2C%22appName%22:%22vendor_client%22%2C%22sessionId%22:%22732d71d3-3eef-4711-a5e2-115e4c956bc3%22%2C%22registeredProps%22:{}}; __ar_v4=TRZ26KFZGFGRPISADZZOI7%3A20181016%3A4%7CB7B7JO3SFNBQJMUA3QHDPS%3A20181016%3A4%7CQOM6IDI7QND4VAHIFCY5QK%3A20181016%3A4; LD_S=1539778340116; existing_merchant=a%2Bbloomsy%40wlfpt.co; LD_L=%7B%22first_name%22%3A%22WolfPoint%22%2C%22last_name%22%3A%22Agency%22%2C%22company%22%3A%22BloomsyBox%22%2C%22email%22%3A%22a%2Bbloomsy%40wlfpt.co%22%7D; __zlcmid=ovhirha4J6W65B; intercom-session-41cc7959d258a6b217d925d6bdccaf896309ba00=RlRBQjFtVlp3SFVRWW8wVllCSTVzbmNub2h6QjV6QUtQMU55dlFuK3A2NXNxYzlxVjJuS2l3N1IxWkwvSXpwSC0tY3hWcXRtS3BLK01tTjVTaWZaU2hUdz09--f64c5d1325fb8fe75067386daf9f490ff43f6882";
	public static String cookieForBloomsy = "session=b'732d71d3-3eef-4711-a5e2-115e4c956bc3'; LD_T=70f82864-dcbd-4d4b-88b7-5f236ecca0b3; _ga=GA1.2.969789885.1539761492; _gid=GA1.2.2107132445.1539761492; statjoy_metrics={%22errors%22:{}%2C%22errorCount%22:0%2C%22callCount%22:{%22init%22:4%2C%22track%22:5%2C%22registerOnce%22:5%2C%22login%22:1}%2C%22successCount%22:{%22init%22:4%2C%22track%22:5%2C%22login%22:1}%2C%22failureCount%22:{}}; LD_U=https%3A%2F%2Fmy.cratejoy.com%2Fv%2Flogin%3Fq%3Dhttps%253A%252F%252Fmy.cratejoy.com%252Fcoupons%252F%23page%3D1; LD_R=; LD_T=70f82864-dcbd-4d4b-88b7-5f236ecca0b3; fs_uid=rs.fullstory.com`5ZK24`6305729055555584:5685265389584384`1634797597`; statjoy_session={%22userId%22:1634797597%2C%22uuid%22:%22e5b69cd2-7437-4c11-bbf9-0f3616dd8b36%22%2C%22appName%22:%22vendor_client%22%2C%22sessionId%22:%22732d71d3-3eef-4711-a5e2-115e4c956bc3%22%2C%22registeredProps%22:{}}; __ar_v4=TRZ26KFZGFGRPISADZZOI7%3A20181016%3A4%7CB7B7JO3SFNBQJMUA3QHDPS%3A20181016%3A4%7CQOM6IDI7QND4VAHIFCY5QK%3A20181016%3A4; LD_S=1539778340116; existing_merchant=a%2Bbloomsy%40wlfpt.co; LD_L=%7B%22first_name%22%3A%22WolfPoint%22%2C%22last_name%22%3A%22Agency%22%2C%22company%22%3A%22BloomsyBox%22%2C%22email%22%3A%22a%2Bbloomsy%40wlfpt.co%22%7D; __zlcmid=ovhirha4J6W65B; intercom-session-41cc7959d258a6b217d925d6bdccaf896309ba00=RlRBQjFtVlp3SFVRWW8wVllCSTVzbmNub2h6QjV6QUtQMU55dlFuK3A2NXNxYzlxVjJuS2l3N1IxWkwvSXpwSC0tY3hWcXRtS3BLK01tTjVTaWZaU2hUdz09--f64c5d1325fb8fe75067386daf9f490ff43f6882";
	public static String cookieForLittlePoppy = "LD_T=08f12988-9207-4ecf-8760-9d8ddf98a3dc; _ga=GA1.2.1855858102.1540884419; fs_uid=rs.fullstory.com`5ZK24`5138431124963328:5629499534213120`1815669541`; statjoy_metrics={%22errors%22:{}%2C%22errorCount%22:0%2C%22callCount%22:{%22init%22:14%2C%22track%22:33%2C%22registerOnce%22:33%2C%22track_resume%22:1%2C%22login%22:2%2C%22logout%22:1}%2C%22successCount%22:{%22init%22:14%2C%22track%22:31%2C%22track_resume%22:1%2C%22login%22:2%2C%22logout%22:1}%2C%22failureCount%22:{%22track%22:1}}; LD_U=https%3A%2F%2Fmy.cratejoy.com%2Fv%2Flogin%3Fq%3Dhttps%253A%252F%252Fmy.cratejoy.com%252Fcoupons%252Fcoupons_list%252F%253Fpage%253D1%2526per_page%253D1000%2526total_pages%253D1%2526total_entries%253D45%2526q%253D; LD_R=; LD_T=08f12988-9207-4ecf-8760-9d8ddf98a3dc; statjoy_session={%22userId%22:1815669541%2C%22uuid%22:%220f02f4e4-6523-43ad-a136-12a788b039d9%22%2C%22appName%22:%22vendor_client%22%2C%22sessionId%22:%2223630292-f87c-4e08-9a09-486a89f9511d%22%2C%22registeredProps%22:{%22$initial_referrer%22:%22https://my.cratejoy.com/refer/%22%2C%22$initial_referrer_href%22:%22https://my.cratejoy.com/refer/%22%2C%22$initial_referring_domain%22:%22my.cratejoy.com%22}}; intercom-session-41cc7959d258a6b217d925d6bdccaf896309ba00=SGsyY0k3SVBaa08vQmpYT01COGIwUzJsUStUdzNSZ1ViS1BYYUY4YnhEenlOTHNISzNXb2liUmRWOWs4QzdhYy0tSTYzdUJMSUNRaXBtUHNZMkJ4UHpqZz09--692e0ae32c4219b6f0b0edecf97f5877098ec32a; __zlcmid=pAhl8c8zGgLGVc; session=b'23630292-f87c-4e08-9a09-486a89f9511d'; _gid=GA1.2.2128193374.1541218026; _fbp=fb.1.1541218026501.1133602583; existing_merchant=a%2Bpoppy%40wlfpt.co; LD_L=%7B%22first_name%22%3A%22Wolf%22%2C%22last_name%22%3A%22Point%22%2C%22company%22%3A%22Little%20Poppy%20Co.%22%2C%22email%22%3A%22a%2Bpoppy%40wlfpt.co%22%7D; last_seen=1541218043045\"";

	public ScrapperController() {
	}

	public void init(ServletConfig Conf) throws ServletException {
		super.init(Conf);
		try {
			servletContext = Conf.getServletContext();

			String log4jConfigFile = servletContext.getInitParameter("log4j-config-location");
			String fullPath = servletContext.getRealPath("") + File.separator + log4jConfigFile;

			org.apache.log4j.PropertyConfigurator.configure(fullPath);
		} catch (Exception ex) {
			logger.error("Failed to get servletContext", ex);
			throw ex;
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println(" In doGet Method");
	}

	private ServletContext servletContext;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String action = request.getParameter("formName");
		System.out.println(" In doPost Method ");

		if (action.equalsIgnoreCase("scrapReview")) {
			doScrapReview(request, response);
		}

		if (action.equalsIgnoreCase("login")) {
			doLogin(request, response);
		}

		if (action.equalsIgnoreCase("CratejoyReview")) {
			doCratejoyReview(request, response);
		}

	}

	private void doScrapReview(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			String fileName = null;
			String handle = request.getParameter("handle");
			String productUrl = request.getParameter("url");
			String url = null;

			if ((handle == null) || (handle.isEmpty())) {
				if ((productUrl == null) || (productUrl.isEmpty())) {
					throw new IllegalArgumentException("Input must not be empty");
				}
				url = productUrl;
				fileName = productUrl.substring(productUrl.indexOf("apps.shopify.com/") + 17,
						productUrl.indexOf("/reviews"));
			} else {
				url = "https://apps.shopify.com/" + handle + "/reviews";
				fileName = handle;
			}

			File output = File.createTempFile("Scrapper", ".xls");

			logger.info(" Scrapper URL - " + url);
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("FirstSheet");

			int i = 1;
			HSSFRow rowhead = sheet.createRow(i);

			rowhead.createCell(1).setCellValue("Reviewer-Name");
			rowhead.createCell(2).setCellValue("Star");
			rowhead.createCell(3).setCellValue("Posted Date");
			rowhead.createCell(4).setCellValue("Review Text");
			rowhead.createCell(5).setCellValue("Suggested Shop URLs");

			Document doc = Jsoup.connect(url).get();

			Elements elements = doc.getElementsByClass("review-listing");
			Elements totalReviewCountElements = doc.getElementsByClass("grid__item gutter-bottom");
			String reviewCount = ((Element) totalReviewCountElements.get(0)).text();
			System.out.println(" Review Count - " + reviewCount);

			int totalReviewCount = Integer
					.parseInt(reviewCount.substring(reviewCount.indexOf("of ") + 3, reviewCount.indexOf(" reviews")));
			logger.info(" Total Review Count - " + totalReviewCount);
			System.out.println(" Total Review Count - " + totalReviewCount);

			int noOfPages = totalReviewCount / 10 + 1;

			for (int j = 1; j <= noOfPages; j++) {
				if (j > 1) {

					if ((handle == null) || (handle.isEmpty())) {
						url = productUrl + "?page=" + j;
					} else {
						url = "https://apps.shopify.com/" + handle + "/reviews?page=" + j;
					}

					doc = Jsoup.connect(url).get();
					elements = doc.getElementsByClass("review-listing");
				}

				for (Element singleEle : elements) {
					HSSFRow row = sheet.createRow(i + 1);
					Elements ele = singleEle.getElementsByClass("review-listing-header__text");
					System.out.println("Reviwer Name - " + ele.first().text());
					row.createCell(1).setCellValue(ele.first().text());

					Elements eleStars = singleEle.getElementsByClass("ui-star-rating__rating visuallyhidden");
					System.out.println("Star - " + eleStars.first().text().substring(0, 1));
					row.createCell(2).setCellValue(eleStars.first().text().substring(0, 1));

					Elements reviewDate = singleEle.getElementsByClass("review-listing-metadata__item-value");
					System.out.println("Posted Date - " + ((Element) reviewDate.get(1)).text());
					row.createCell(3).setCellValue(((Element) reviewDate.get(1)).text());

					Elements reviewTextElements = singleEle
							.getElementsByClass("review-listing-body__content truncate-content-copy");
					Elements getPTagElements = ((Element) reviewTextElements.get(0)).getElementsByTag("P");
					StringBuffer reviewText = new StringBuffer("");
					for (Element singleReviewTextElement : getPTagElements) {
						reviewText = reviewText.append(singleReviewTextElement.text());
					}
					System.out.println("Review Text - " + reviewText.toString());
					row.createCell(4).setCellValue(reviewText.toString());

					try {
						String storeName = ele.first().text();
						if (storeName.contains(".")) {
							storeName = "https://" + storeName;
						} else {
							storeName = storeName.replace(" ", "-");
							storeName = storeName.replace(",", "");
							storeName = storeName.replace("'", "");
							storeName = "https://" + storeName;
							storeName = storeName + ".myshopify.com";
						}

						System.out.println(" URL - " + storeName);

						Jsoup.connect(storeName).get();
						row.createCell(5).setCellValue(storeName);

						System.out.println(" Success ");
					} catch (Exception e) {
						logger.error("Error while getting Shop URL ", e);
						System.out.println("Error while getting Shop URL - " + ele.first().text() + " - Error - "
								+ e.getMessage());
					}
					i++;
				}
			}

			FileOutputStream fileOut = new FileOutputStream(output);
			workbook.write(fileOut);
			fileOut.close();

			InputStream is = new FileInputStream(output);
			String mimeType = "application/octet-stream";

			response.setContentType(mimeType);
			response.setContentLength(is.available());

			String headerKey = "Content-Disposition";
			String filename = fileName + "_Review" + ".xls";
			String headerValue = String.format("attachment; filename=\"%s\"", new Object[] { filename });
			response.setHeader(headerKey, headerValue);

			OutputStream outStream = response.getOutputStream();

			byte[] buffer = new byte[2048];
			int bytesRead = -1;

			while ((bytesRead = is.read(buffer)) != -1) {
				outStream.write(buffer, 0, bytesRead);
			}
			is.close();
			outStream.close();
			logger.info("Your excel file has been generated!");
		} catch (HttpStatusException | StringIndexOutOfBoundsException e) {
			logger.error("Error while generating file", e);

			request.getSession().setAttribute("responseType", "fail");
			request.getSession().setAttribute("responseMessage", "Please provide valid URL.");
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		} catch (Exception e) {
			logger.error("Error while generating file", e);

			request.getSession().setAttribute("responseType", "fail");
			request.getSession().setAttribute("responseMessage", e.getMessage());

			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}

	}

	private void doCratejoyReview(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String product = request.getParameter("product");
		File output = File.createTempFile(product+"_coupons", ".xlsx");
		
		OutputStream outStream = response.getOutputStream();
		
//		args = new String[1];
//		args[0] = "E:\\coupons.csv";

		BufferedWriter out = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(output.getPath().replaceAll("\"", "")), "UTF8"));
		
		byte[] buffer = new byte[2048];
		int bytesRead = -1;
		InputStream is = new FileInputStream(output);
		String header = "Total Redeemed,Description,Coupon Code,Active?,Coupon Name,URL,Discount Type,Free Shipping,Min Order Price,Multiplier (%),Reduction ($ Amount),Activation Date,Expiration Date,Max Redemptions,Once Per Customer,One Item Per Cart,Exclusive,# of Renewals,Recurring Type,Selected Products\r\n";
		out.write(header, 0, header.length());
//		out.write(
//				"Total Redeemed,Description,Coupon Code,Active?,Coupon Name,URL,Discount Type,Free Shipping,Min Order Price,Multiplier (%),Reduction ($ Amount),Activation Date,Expiration Date,Max Redemptions,Once Per Customer,One Item Per Cart,Exclusive,# of Renewals,Recurring Type,Selected Products\r\n");

		int counter = 0;
		String h = getHTML("https://my.cratejoy.com/coupons/coupons_list/?page=1&per_page=1000&total_pages=1&total_entries=45&q=",product);
		h += "{\"all_sub_prod_enabled";
		while (true) {
			String row = extractDataWithHTML(h, "{\"all_sub_prod_enabled", "{\"all_sub_prod_enabled");
			if (row.equals(""))
				break;
			h = h.substring(h.indexOf("{\"all_sub_prod_enabled") + 5);

			List<String> list = new ArrayList<String>();
			
			list.add(extractData(row, "\"total_redeemed\":", ","));
			list.add(extractData(row, "\"description\": \"", "\""));
			list.add(extractData(row, "\"primary_code\": \"", "\""));
			list.add(extractData(row, "\"active\":", ","));
			list.add(extractData(row, "\"name\": \"", "\""));

			String id = extractData(row, "\"coupon_id\":", "}");
			list.add("https://my.cratejoy.com/coupon/" + id);
			String h1 = getHTML("https://my.cratejoy.com/coupon/" + id,product);

			h1 = extractDataWithHTML(h1, "window.coupon =", "window.cj.has_ecom");
			String h2 = extractDataWithHTML(h1, "\"discount\": {", "}");

			String type = extractData(h2, "\"discount_type\":", ",");
			if (type.equals("1"))
				type = "No Discount";
			else if (type.equals("3"))
				type = "Percentage";
			else if (type.equals("2"))
				type = "Flat Rate";
			else if (type.equals("4"))
				type = "Min Order Price";
			else if (type.equals("5"))
				type = "Free Shipping";

			list.add(type);
			list.add(extractData(h2, "\"free_shipping\":", ","));
			list.add(extractData(h2, "\"min_price\":", ","));
			list.add(extractData(h2, "\"multiplier\":", ","));
			list.add(extractData(h2, "\"reduction\":", ","));

			h2 = extractDataWithHTML(h1, "\"limits\": {", "}");
			String date = extractData(h2, "\"activation_date\":", ",");
			String active = "";
			try {
				Date dt = new Date(Long.parseLong(date));

				String DATE_FORMAT2 = "MM/dd/yyyy";
				java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT2);

				active = sdf.format(dt);

			} catch (Exception ex) {

			}

			date = extractData(h2, "\"expiration_date\":", ",");
			String exp = "";
			try {
				Date dt = new Date(Long.parseLong(date));

				String DATE_FORMAT2 = "MM/dd/yyyy";
				java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(DATE_FORMAT2);

				exp = sdf.format(dt);
			} catch (Exception ex) {

			}

			list.add(active);
			list.add(exp);

			String max = extractData(h2, "\"max_redemptions\":", ",");
			if (max.equals("null"))
				max = "";
			list.add(max);
			list.add(extractData(h2, "\"once_per_customer\":", ","));
			list.add(extractData(h2, "\"one_item_per_cart\":", ","));
			list.add(extractData(h2, "\"exclusive\":", ","));

			h2 = extractDataWithHTML(h1, "\"recurring\": {", "}");
			list.add(extractData(h2, "\"num_renewals\":", ","));
			String rType = extractData(h2, "\"recurring_type\":", ",");
			if (rType.equals("1"))
				rType = "Apply only once";
			else if (rType.equals("2"))
				rType = "Apply on every renewal";
			else if (rType.equals("3"))
				rType = "Fixed number of renewals";

			list.add(rType);

			h1 += "{\"coupon_id\":";
			String subscription = "", oneTime = "";
			while (true) {
				String row2 = extractDataWithHTML(h1, "{\"coupon_id\":", "{\"coupon_id\":");
				if (row2.equals(""))
					break;
				h1 = h1.substring(h1.indexOf("{\"coupon_id\":") + 5);

				String isEcom = extractData(row2, "\"is_ecom\":", ",");
				String productName = extractData(row2, "product_name\": \"", "\"");
				row2 = extractDataWithHTML(row2, "\"terms\": [", "]");
				int trueCount = 0, falseCount = 0;

				while (true) {
					String row3 = extractDataWithHTML(row2, "{", "}");
					if (row3.equals(""))
						break;
					row2 = row2.substring(row2.indexOf("{") + 5);

					String enabled = extractData(row3, "enabled\":", ",");
					if (enabled.equals("true"))
						trueCount++;
					else if (enabled.equals("false"))
						falseCount++;
				}
				if (trueCount > 0 && falseCount == 0) {
					if (isEcom.equals("false")) {
						subscription += productName + ", ";
					} else
						oneTime += productName + ", ";
				}
			}

			subscription = subscription.trim();
			oneTime = oneTime.trim();

			if (subscription.endsWith(","))
				subscription = subscription.substring(0, subscription.length() - 1).trim();
			if (oneTime.endsWith(","))
				oneTime = oneTime.substring(0, oneTime.length() - 1).trim();

			list.add(subscription);
			list.add(oneTime);

			String line = "";
			for (int k = 0; k < list.size(); k++) {
				String l = (String) list.get(k);
				line += "\"" + l.replaceAll("\"", "\"\"") + "\",";
			}

			if (line.endsWith(","))
				line = line.substring(0, line.length() - 1).trim();

			line = line + "\r\n";
			
			out.write(line, 0, line.length());

			counter++;
			System.out.println(counter);
		}
		
		out.close();
	
		//FileInputStream fileIn = new FileInputStream(output);
		

		InputStream fileIn = new FileInputStream(output);
		String mimeType = "application/octet-stream";

		response.setContentType(mimeType);
		response.setContentLength(fileIn.available());

		String headerKey = "Content-Disposition";
		String filename = product + "_Review" + ".csv";
		String headerValue = String.format("attachment; filename=\"%s\"", new Object[] { filename });
		response.setHeader(headerKey, headerValue);

		

		//byte[] buffer = new byte[2048];
		//int bytesRead = -1;

		while ((bytesRead = fileIn.read(buffer)) != -1) {
			outStream.write(buffer, 0, bytesRead);
		}
		is.close();
		fileIn.close();
		outStream.close();
		logger.info("Your excel coupen file has been generated!");
	}

	private void doLogin(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String userName = request.getParameter("usernm");

		String passWord = request.getParameter("passwd");

		if (userName.equals("admin") && passWord.equals("#Figure8out")) {
			request.getSession().setAttribute("username", userName);
			request.getSession().setAttribute("password", passWord);
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		} else if (userName.equals("admin")) {
			request.getSession().setAttribute("responseType", "fail");
			request.getSession().setAttribute("responseMessage", "Please enter valid password.");
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		} else {
			request.getSession().setAttribute("responseType", "fail");
			request.getSession().setAttribute("responseMessage", "Please enter valid username and password.");
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		}

	}

	private Set<String> getDataFromGoogle(String query) {

		Set<String> result = new HashSet<String>();
		String request = "https://www.google.com/search?q=" + query + "&num=20";
		System.out.println("Sending request..." + request);

		try {

			// need http protocol, set this as a Google bot agent :)
			Document doc = Jsoup.connect(request)
					.userAgent("Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)").timeout(5000)
					.get();

			// get all links
			Elements links = doc.select("a[href]");
			for (Element link : links) {

				String temp = link.attr("href");
				if (temp.startsWith("/url?q=")) {
					// use regex to get domain name
					result.add(getDomainName(temp));
				}

			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	public String getDomainName(String url) {

		String domainName = "";
		matcher = patternDomainName.matcher(url);
		if (matcher.find()) {
			domainName = matcher.group(0).toLowerCase().trim();
		}
		return domainName;

	}
	
	public static String extractDataUrl(String html, String s2, String s3) {
		int i = html.indexOf(s2);
		String result = "";
		if (i >= 0) {
			html = html.substring(i + s2.length());
			int j = html.indexOf(s3);
			if (j >= 0) {
				result = html.substring(0, j);
			} else {
				return "";
			}
		}

		return result.replaceAll("\\<.*?\\>", "").replaceAll("\\s+", " ").trim();
	}

	public static String extractData(String html, String s2, String s3) {
		int i = html.indexOf(s2);
		String result = "";
		if (i >= 0) {
			html = html.substring(i + s2.length());
			int j = html.indexOf(s3);
			if (j >= 0) {
				result = html.substring(0, j);
			} else {
				return "";
			}
		}

		return result.replaceAll("&nbsp;", " ").replaceAll("\\<.*?\\>", "").replaceAll("\\s+", " ").trim();
	}

	public static String extractDataWithHTML(String html, String s2, String s3) {
		int i = html.indexOf(s2);
		String result = "";
		if (i >= 0) {
			html = html.substring(i + s2.length());
			int j = html.indexOf(s3);
			if (j >= 0) {
				result = html.substring(0, j);
			} else {
				return "";
			}
		}

		return result.replaceAll("&nbsp;", " ").replaceAll("\\s+", " ").trim();
	}

	public static String getHTML(String address,String product) {

		String html = "";
		for (int j = 0; j < 3; j++) {
			try {
				// Thread.sleep(2000);
				URLConnection conn = null;

				html = "";

				URL url = new URL(address);

				conn = url.openConnection();

				conn.setRequestProperty("User-Agent",
						"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:55.0) Gecko/20100101 Firefox/55.0");
				if (product.contains("bloomsy")) {
					conn.setRequestProperty("Cookie", cookieForBloomsy);
				}

				if (product.contains("poppy")) {
					conn.setRequestProperty("Cookie", cookieForLittlePoppy);
				}

				try {
					InputStream is = conn.getInputStream();
					final Reader reader = new InputStreamReader(is, "UTF-8");
					final char[] buf = new char[16384];
					int read;
					final StringBuffer sb = new StringBuffer();
					while ((read = reader.read(buf)) > 0) {
						sb.append(buf, 0, read);
					}

					html = sb.toString();
				} finally {

				}

				break;
			} catch (Exception ex) {
				// ex.printStackTrace();
				if (ex.getClass().toString().indexOf("FileNotFoundException") >= 0) {
					return "XXX";
				}
			}
		}

		return html;
	}
}
