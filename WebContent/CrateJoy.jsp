<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>CrateJoy</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<script>
	$(function() {
		$("#datepicker").datepicker();
	});
</script>
</head>
<body>

	<div class="container"
		style="border: 1px solid #888888; margin-top: 100px; max-width: 800px; box-shadow: 1px 1px #888888">
		<h2 style="margin-top: 10px; margin-bottom: 30px;">
			<center>CrateJoy</center>
		</h2>
		<form method="post" action="CrateJoy">
			<div class="row">
					<div class="col-lg-8">
								 <input type="text" class="form-control"
							placeholder="Date" name="datepicker" id="datepicker">
					</div>
					<div class="col-lg-3">
						<div class="input-group mb-3">
							<input type="submit" class="btn btn-primary">
						</div>
					</div>
				<div class="col-lg-12">
					<p align="center">OR</p>
					<br>
				</div>

				<div class="col-lg-12">Fetch Product Reviews</div>

				<div class="col-lg-8" style="margin-bottom: 50px">
					<div class="input-group mb-3">
						<input type="url" class="form-control"
							pattern="https://apps.shopify.com/.+"
							placeholder="Enter Product URL" name="url">
					</div>
				</div>
				<div class="col-lg-3">
					<div class="input-group mb-3">
						<input type="submit" class="btn btn-primary">
					</div>
				</div>
				<input type="hidden" name="formName" value="scrapReview">
			</div>
		</form>
		<div class="col-lg-12">
			<p align="center">OR</p>
			<br>
		</div>

		<div class="row">
			<div class="col-lg-4"></div>
			<div class="col-lg-4">
				<form method="post" action="Scrap">
					<div class="input-group mb-3">
						<input type="submit" class="btn btn-primary"
							value="Get Cratejoy Review"> <input type="hidden"
							name="formName" value="CratejoyReview">
					</div>
				</form>
			</div>
		</div>
	</div>



	<c:if test="${!empty responseMessage}">
		<c:if test="${responseType == 'fail'}">
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-md">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Action
								Successful:</h5>
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" align="center">
							<img id="responseImage" src="images/xMark.png"
								alt="response message" />
							<p id="responseMessage">
								<br>
								<c:out value="${responseMessage}" />
							</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary"
								data-dismiss="modal">Close</button>
						</div>
					</div>
				</div>
			</div>
		</c:if>
	</c:if>
	<script>
		$("#exampleModal").modal("show");
	</script>
	<c:remove var="responseMessage" />
	<c:remove var="responseType" />
</body>
</html>